﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;

namespace Serializator
{
    public static class SerializatorExt
    {
        public static string ToCsv<T>(this T SerializObject) =>
            string.Join(", ", SerializObject.GetType().GetProperties().Select(s => s.Name + "=" + s.GetValue(SerializObject)));

        public static T FromCsv<T>(string inputStr) where T : new()
        {
            T result = new T();
            var type = result.GetType();
            string pattern=@"""([a-zA-Z0-9]+)"":(\d+)";
            foreach (Match match in Regex.Matches(inputStr, pattern))
            {
                var propertyInfo = type.GetProperty(match.Groups[1].Value);
                propertyInfo.SetValue(result, Convert.ToInt32(match.Groups[2].Value));
            }            
            return result;
        }

    }
}
