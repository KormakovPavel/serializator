﻿using System;
using System.Text.Json;
namespace Serializator
{
    class Program
    {
        static void Main(string[] args)
        {           
            var SerializObject = F.Get();
            string strSerializ = "";

            Console.WriteLine("Сериализация:");
            DateTime DateBeginSerializ = DateTime.UtcNow;
            for (int i = 0; i < 1_000_000; i++)
                strSerializ=SerializObject.ToCsv();
            DateTime DateEndSerializ = DateTime.UtcNow;             
            
            DateTime DateBeginOut = DateTime.UtcNow;
            Console.WriteLine($"Мой рефлекшен:\n{strSerializ}\nВремя сериализации: {(DateEndSerializ - DateBeginSerializ).TotalMilliseconds} мс");
            DateTime DateEndOut = DateTime.UtcNow;
            Console.WriteLine($"Время вывода: {(DateEndOut - DateBeginOut).TotalMilliseconds} мс");

            DateBeginSerializ = DateTime.UtcNow;
            for (int i = 0; i < 1_000_000; i++)
                strSerializ = JsonSerializer.Serialize(SerializObject);
            DateEndSerializ = DateTime.UtcNow;
            Console.WriteLine($"Json рефлекшен:\n{strSerializ}\nВремя сериализации: {(DateEndSerializ - DateBeginSerializ).TotalMilliseconds} мс");

            F DeserializObject=null;
            Console.WriteLine("\nДесериализация:");
            DateBeginSerializ = DateTime.UtcNow;
            for (int i = 0; i < 1_000_000; i++)
                DeserializObject = SerializatorExt.FromCsv<F>(strSerializ);
            DateEndSerializ = DateTime.UtcNow;
            Console.WriteLine($"Время моей десериализации: {(DateEndSerializ - DateBeginSerializ).TotalMilliseconds} мс");

            DateBeginSerializ = DateTime.UtcNow;
            for (int i = 0; i < 1_000_000; i++)
                DeserializObject = JsonSerializer.Deserialize<F>(strSerializ);
            DateEndSerializ = DateTime.UtcNow;
            Console.WriteLine($"Время Json десериализация: {(DateEndSerializ - DateBeginSerializ).TotalMilliseconds} мс");

            Console.ReadLine();
        }
    }
}
